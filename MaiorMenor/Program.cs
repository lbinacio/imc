﻿using System;

namespace MaiorMenor
{
    class Program
    {
        static void Main(string[] args)
        {

            MaiorOuMenor maiorOuMenor = new MaiorOuMenor();

            maiorOuMenor.Num1 = 1;
            maiorOuMenor.Num2 = 3;
            maiorOuMenor.Num3 = 3;

            // 321 - ok
            // 123 - ok
            // 231 - ok
            // 312 - ok
            // 213 - ok
            // 132 - ok

            int maior = maiorOuMenor.Maior();
            Console.WriteLine(maior);

            int menor = maiorOuMenor.Menor();
            Console.WriteLine(menor);

        }
    }
    }


