using NUnit.Framework;
using calculadora;

namespace CalculadoraTests
{
    
    public class Tests
    {
        [SetUp]
        public void Setup()//nome pro metodo nome da funcao que vai testar, cenario, VOID VAZIO
        {
        }

        [Test]
        public void ExecutarCalculo_QuandoAcaoForSomar_RetornarSoma()//apos o _ "underline se chama cenario, apos segundo _ oque espera oque o cenario retorne
        {
            //arrange "preparar"

            var calculadora = new Calculadora();//calculadora deu erro quando foi criada, ai foi preciso dar using la em cima com nome calculadora, ainda deu erro foi preciso dar uma referencia
            //calculadora. siginifca que esta no namespace calculadora "projeto" e depois do ponto esta na classe, indicando o caminho que esta fazendo
            calculadora.numA = 1;
            calculadora.numB = 2;
            calculadora.operacao = "somar";


            //act "chamar acao
            var resultado = calculadora.ExecutarCalculo(); //Executar � o metodo


            //assert =  � fazer comparacao para ver se o resultado � o esperado


            Assert.That(resultado, Is.EqualTo(3));
        }
        [Test]
        public void ExecutarCalculo_QuandoAcaoForSubtrair_RetornarSubtracao()//apos o _ "underline se chama cenario, apos segundo _ oque espera oque o cenario retorne
        {
            //arrange "preparar"

            var calculadora = new Calculadora();//calculadora deu erro quando foi criada, ai foi preciso dar using la em cima com nome calculadora, ainda deu erro foi preciso dar uma referencia
            //calculadora. siginifca que esta no namespace calculadora "projeto" e depois do ponto esta na classe, indicando o caminho que esta fazendo
            calculadora.numA = 1;
            calculadora.numB = 2;
            calculadora.operacao = "subtrair";


            //act "chamar acao
            var resultado = calculadora.ExecutarCalculo(); //Executar � o metodo


            //assert =  � fazer comparacao para ver se o resultado � o esperado


            Assert.That(resultado, Is.EqualTo(-1));
        }
        [Test]
        public void ExecutarCalculo_QuandoAcaoForMultiplicar_RetornarMultiplicacao()//apos o _ "underline se chama cenario, apos segundo _ oque espera oque o cenario retorne
        {
            //arrange "preparar"

            var calculadora = new Calculadora();//calculadora deu erro quando foi criada, ai foi preciso dar using la em cima com nome calculadora, ainda deu erro foi preciso dar uma referencia
            //calculadora. siginifca que esta no namespace calculadora "projeto" e depois do ponto esta na classe, indicando o caminho que esta fazendo
            calculadora.numA = 1;
            calculadora.numB = 2;
            calculadora.operacao = "multiplicacao";


            //act "chamar acao
            var resultado = calculadora.ExecutarCalculo(); //Executar � o metodo


            //assert =  � fazer comparacao para ver se o resultado � o esperado


            Assert.That(resultado, Is.EqualTo(2));
        }
        [Test]
        public void ExecutarCalculo_QuandoAcaoForDividir_RetornarDividir()//apos o _ "underline se chama cenario, apos segundo _ oque espera oque o cenario retorne
        {
            //arrange "preparar"

            var calculadora = new calculadora.Calculadora();//calculadora deu erro quando foi criada, ai foi preciso dar using la em cima com nome calculadora, ainda deu erro foi preciso dar uma referencia
            //calculadora. siginifca que esta no namespace calculadora "projeto" e depois do ponto esta na classe, indicando o caminho que esta fazendo
            calculadora.numA = 1;
            calculadora.numB = 0;
            calculadora.operacao = "dividir";


            //act "chamar acao
            var resultado = calculadora.ExecutarCalculo(); //Executar � o metodo


            //assert =  � fazer comparacao para ver se o resultado � o esperado


            Assert.That(resultado, Is.EqualTo(0));
        }
    }
}
